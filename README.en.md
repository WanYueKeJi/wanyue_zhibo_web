# 万岳直播商城系统服务端+后台web版

#### Description
万岳直播商城系统服务web端，后端web+前端ios、android，适用于直播商城、短视频商城、社区商城、知识付费等形式，该开源版包含多商户入驻、直播带货、主播分销、电商商城等功能，后续将继续开源小程序端，欢迎关注学习！

#### Software Architecture
Software architecture description

#### Installation


1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
