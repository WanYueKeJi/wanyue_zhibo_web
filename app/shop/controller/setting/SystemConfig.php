<?php

namespace app\shop\controller\setting;

use app\shop\controller\AuthController;
use app\shop\model\setting\ShopConfig;
use wanyue\services\{
    UtilService as Util,
    JsonService as Json
};
use wanyue\services\upload\Upload;


/**
 *  配置列表控制器
 * Class SystemConfig
 * @package app\admin\controller\system
 */
class SystemConfig extends AuthController
{
    /**
     * 基础配置
     * */
    public function index()
    {
        $shopid=$this->shopId;
        $config=ShopConfig::getC($shopid);

        $this->assign(compact('config'));
        return $this->fetch();
    }

    /**
     * 保存
     */
    public function update_config()
    {
        $data = request()->post();
        $shopid=$this->shopId;
        ShopConfig::setC($shopid,$data);

        return Json::successful('修改成功!');
    }

    /**
     * 文件上传
     * */
    public function file_upload()
    {
        $upload = new Upload('local');
        $res = $upload->to('config/file')->validate()->move($this->request->param('file', 'file'));
        if ($res === false) return Json::fail($upload->getError());
        return Json::successful('上传成功!', ['filePath' => $res->filePath]);
    }

}
