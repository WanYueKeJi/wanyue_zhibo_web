<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/11/11
 */

namespace app\admin\controller\user;

use app\admin\controller\AuthController;
use wanyue\repositories\UserRepository;
use wanyue\traits\CurdControllerTrait;
use think\facade\Route as Url;
use wanyue\basic\BaseModel;
use app\admin\model\order\StoreOrder;
use app\admin\model\wechat\WechatMessage;
use app\admin\model\store\{StoreVisit};
use wanyue\services\{FormBuilder as Form, UtilService as Util, JsonService as Json};
use app\admin\model\user\{User as UserModel, UserBill as UserBillAdmin, UserGroup,UserSuper};

/**
 * 用户管理控制器
 * Class User
 * @package app\admin\controller\user
 */
class User extends AuthController
{
    use CurdControllerTrait;

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {

        $group = UserGroup::select();
        $this->assign(compact('group'));
        $this->assign('count_user', UserModel::getcount());
        return $this->fetch();
    }

    /**
     * 设置分组
     * @param int $uid
     */
    public function set_group($uid = 0)
    {
        if (!$uid) return $this->failed('缺少参数');
        $userGroup = UserGroup::select();
        $field[] = Form::select('group_id', '会员分组')->setOptions(function () use ($userGroup) {
            $menus = [];
            foreach ($userGroup as $menu) {
                $menus[] = ['value' => $menu['id'], 'label' => $menu['group_name']];
            }
            return $menus;
        })->filterable(1);
        $form = Form::make_post_form('设置分组', $field, Url::buildUrl('save_set_group', ['uid' => $uid]), 2);
        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    public function save_set_group($uid = 0)
    {
        if (!$uid) return Json::fail('缺少参数');
        list($group_id) = Util::postMore([
            ['group_id', 0],
        ], $this->request, true);
        $uids = explode(',',$uid);
        $res = UserModel::whereIn('uid', $uids)->update(['group_id' => $group_id]);
        if ($res) {
            return Json::successful('设置成功');
        } else {
            return Json::successful('设置失败');
        }
    }

    public function edit_other($uid)
    {
        if (!$uid) return $this->failed('数据不存在');
        $user = UserModel::get($uid);
        if (!$user) return Json::fail('数据不存在!');
        $f = array();
        $f[] = Form::radio('money_status', '修改余额', 1)->options([['value' => 1, 'label' => '增加'], ['value' => 2, 'label' => '减少']]);
        $f[] = Form::number('money', '余额')->min(0);
        $f[] = Form::radio('integration_status', '修改积分', 1)->options([['value' => 1, 'label' => '增加'], ['value' => 2, 'label' => '减少']]);
        $f[] = Form::number('integration', '积分')->min(0);
        $form = Form::make_post_form('修改其他', $f, Url::buildUrl('update_other', array('uid' => $uid)));
        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    public function update_other($uid = 0)
    {
        $data = Util::postMore([
            ['money_status', 0],
            ['money', 0],
            ['integration_status', 0],
            ['integration', 0],
        ], $this->request);
        if (!$uid) return $this->failed('数据不存在');
        $user = UserModel::get($uid);
        if (!$user) return Json::fail('数据不存在!');
        BaseModel::beginTrans();
        $res1 = false;
        $res2 = false;
        $edit = array();
        if ($data['money_status'] && $data['money']) {//余额增加或者减少
            if ($data['money_status'] == 1) {//增加
                $edit['now_money'] = bcadd($user['now_money'], $data['money'], 2);
                $res1 = UserBillAdmin::income('系统增加余额', $user['uid'], 'now_money', 'system_add', $data['money'], $this->adminId, $edit['now_money'], '系统增加了' . floatval($data['money']) . '余额');
                try {
                    UserRepository::adminAddMoney($user, $data['money']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            } else if ($data['money_status'] == 2) {//减少
                $edit['now_money'] = bcsub($user['now_money'], $data['money'], 2);
                $res1 = UserBillAdmin::expend('系统减少余额', $user['uid'], 'now_money', 'system_sub', $data['money'], $this->adminId, $edit['now_money'], '系统扣除了' . floatval($data['money']) . '余额');
                try {
                    UserRepository::adminSubMoney($user, $data['money']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            }
        } else {
            $res1 = true;
        }
        if ($data['integration_status'] && $data['integration']) {//积分增加或者减少
            if ($data['integration_status'] == 1) {//增加
                $edit['integral'] = bcadd($user['integral'], $data['integration'], 2);
                $res2 = UserBillAdmin::income('系统增加积分', $user['uid'], 'integral', 'system_add', $data['integration'], $this->adminId, $edit['integral'], '系统增加了' . floatval($data['integration']) . '积分');
                try {
                    UserRepository::adminAddIntegral($user, $data['integration']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            } else if ($data['integration_status'] == 2) {//减少
                $edit['integral'] = bcsub($user['integral'], $data['integration'], 2);
                $res2 = UserBillAdmin::expend('系统减少积分', $user['uid'], 'integral', 'system_sub', $data['integration'], $this->adminId, $edit['integral'], '系统扣除了' . floatval($data['integration']) . '积分');
                try {
                    UserRepository::adminSubIntegral($user, $data['integration']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            }
        } else {
            $res2 = true;
        }
        if ($edit) $res3 = UserModel::edit($edit, $uid);
        else $res3 = true;
        if ($res1 && $res2 && $res3) $res = true;
        else $res = false;
        BaseModel::checkTrans($res);
        if ($res) return Json::successful('修改成功!');
        else return Json::fail('修改失败');
    }

    /**
     * 修改user表状态
     *
     * @return json
     */
    public function set_status($status = '', $uid = 0, $is_echo = 0)
    {
        if ($is_echo == 0) {
            if ($status == '' || $uid == 0) return Json::fail('参数错误');
            UserModel::where(['uid' => $uid])->update(['status' => $status]);
            if($status==0){
                $uids=[$uid];
                $this->setUnStatus($uids);
            }


        } else {
            $uids = Util::postMore([
                ['uids', []]
            ]);
            UserModel::destrSyatus($uids['uids'], $status);

            if($status==0){
                $this->setUnStatus($uids['uids']);
            }
        }
        return Json::successful($status == 0 ? '禁用成功' : '解禁成功');
    }

    /**
     * 获取user表
     *
     * @return json
     */
    public function get_user_list()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['nickname', ''],
            ['status', ''],
            ['pay_count', ''],
            ['order', ''],
            ['data', ''],
            ['user_type', ''],
            ['country', ''],
            ['province', ''],
            ['city', ''],
            ['user_time_type', ''],
            ['user_time', ''],
            ['sex', ''],
            ['group_id', ''],
        ]);
        return Json::successlayui(UserModel::getUserList($where));
    }
    public function set_super($uid,$status=0)
    {
		
		$data=UserSuper::setstatus($uid,$status);
		if($data==0){
			return Json::fail('修改失败!');
		}elseif($data==1){
			return Json::successful('设置成功!');
		}else{
			return Json::successful('取消成功!');
		}
		
    }	

    /**
     * 编辑模板消息
     * @param $id
     * @return mixed|\think\response\Json|void
     */
    public function edit($uid)
    {
        if (!$uid) return $this->failed('数据不存在');
        $user = UserModel::get($uid);
        if (!$user) return Json::fail('数据不存在!');
        $f = array();
        $f[] = Form::input('uid', '用户编号', $user->getData('uid'))->disabled(1);
        $f[] = Form::input('nickname', '昵称', $user->getData('nickname'));
        $f[] = Form::frameImageOne('avatar', '图标(180*180)', Url::buildUrl('admin/widget.images/index', array('fodder' => 'avatar')),$user->getData('avatar'))->icon('image')->width('100%')->height('500px');
        $f[] = Form::input('real_name', '真实姓名', $user->getData('real_name'));
        $f[] = Form::text('phone', '手机号', $user->getData('phone'));
        $f[] = Form::date('birthday', '生日', $user->getData('birthday') ? date('Y-m-d', $user->getData('birthday')) : 0);
        $f[] = Form::input('card_id', '身份证号', $user->getData('card_id'));
        $f[] = Form::textarea('mark', '用户备注', $user->getData('mark'));

        $f[] = Form::radio('status', '状态', $user->getData('status'))->options([['value' => 1, 'label' => '开启'], ['value' => 0, 'label' => '锁定']]);
        $form = Form::make_post_form('添加用户通知', $f, Url::buildUrl('update', array('uid' => $uid)), 5);
        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    public function update($uid)
    {
        $data = Util::postMore([
            ['money_status', 0],
            ['nickname', ''],
            ['real_name', ''],
            ['avatar', ''],
            ['phone', 0],
            ['card_id', ''],
            ['birthday', ''],
            ['mark', ''],
            ['money', 0],
            ['integration_status', 0],
            ['integration', 0],
            ['status', 0],
        ]);
        if (!$uid) return $this->failed('数据不存在');
        $user = UserModel::get($uid);
        if (!$user) return Json::fail('数据不存在!');
        BaseModel::beginTrans();
        $res1 = false;
        $res2 = false;
        $edit = array();
        if ($data['money_status'] && $data['money']) {//余额增加或者减少
            if ($data['money_status'] == 1) {//增加
                $edit['now_money'] = bcadd($user['now_money'], $data['money'], 2);
                $res1 = UserBillAdmin::income('系统增加余额', $user['uid'], 'now_money', 'system_add', $data['money'], $this->adminId, $edit['now_money'], '系统增加了' . floatval($data['money']) . '余额');
                try {
                    UserRepository::adminAddMoney($user, $data['money']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            } else if ($data['money_status'] == 2) {//减少
                $edit['now_money'] = bcsub($user['now_money'], $data['money'], 2);
                $res1 = UserBillAdmin::expend('系统减少余额', $user['uid'], 'now_money', 'system_sub', $data['money'], $this->adminId, $edit['now_money'], '系统扣除了' . floatval($data['money']) . '余额');
                try {
                    UserRepository::adminSubMoney($user, $data['money']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            }
        } else {
            $res1 = true;
        }
        if ($data['integration_status'] && $data['integration']) {//积分增加或者减少
            if ($data['integration_status'] == 1) {//增加
                $edit['integral'] = bcadd($user['integral'], $data['integration'], 2);
                $res2 = UserBillAdmin::income('系统增加积分', $user['uid'], 'integral', 'system_add', $data['integration'], $this->adminId, $edit['integral'], '系统增加了' . floatval($data['integration']) . '积分');
                try {
                    UserRepository::adminAddIntegral($user, $data['integration']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            } else if ($data['integration_status'] == 2) {//减少
                $edit['integral'] = bcsub($user['integral'], $data['integration'], 2);
                $res2 = UserBillAdmin::expend('系统减少积分', $user['uid'], 'integral', 'system_sub', $data['integration'], $this->adminId, $edit['integral'], '系统扣除了' . floatval($data['integration']) . '积分');
                try {
                    UserRepository::adminSubIntegral($user, $data['integration']);
                } catch (\Exception $e) {
                    BaseModel::rollbackTrans();
                    return Json::fail($e->getMessage());
                }
            }
        } else {
            $res2 = true;
        }
        $edit['status'] = $data['status'];
        $edit['nickname'] = $data['nickname'];
        $edit['real_name'] = $data['real_name'];
        $edit['avatar'] = $data['avatar'];
        $edit['phone'] = $data['phone'];
        $edit['card_id'] = $data['card_id'];
        $edit['birthday'] = strtotime($data['birthday']);
        $edit['mark'] = $data['mark'];
        if ($edit) $res3 = UserModel::edit($edit, $uid);
        else $res3 = true;
        if ($res1 && $res2 && $res3) $res = true;
        else $res = false;
        BaseModel::checkTrans($res);
        if($res && $data['status']==0){
            $uids=[$uid];
            $this->setUnStatus($uids);
        }
        if ($res){
            $data_redis=[
                'nickname'=>$edit['nickname'],
                'avatar'=>$edit['avatar'],
            ];
            \app\models\user\User::upUserInfoByRedis($uid,$data_redis);
            return Json::successful('修改成功!');
        }
        else return Json::fail('修改失败');
    }

    /**
     * 用户图表
     * @return mixed
     */

    public function gettime($time = '', $season = '')
    {
        if (!empty($time) && empty($season)) {
            $timestamp0 = strtotime($time);
            $timestamp24 = strtotime($time) + 86400;
            return [$timestamp0, $timestamp24];
        } else if (!empty($time) && !empty($season)) {
            $firstday = date('Y-m-01', mktime(0, 0, 0, ($season - 1) * 3 + 1, 1, date('Y')));
            $lastday = date('Y-m-t', mktime(0, 0, 0, $season * 3, 1, date('Y')));
            return [$firstday, $lastday];
        }
    }

    /**
     * 会员等级首页
     */
    public function group()
    {
        return $this->fetch();
    }

    /**
     * 会员详情
     */
    public function see($uid = '')
    {
        $this->assign([
            'uid' => $uid,
            'userinfo' => UserModel::getUserDetailed($uid),
            'is_layui' => true,
            'headerList' => UserModel::getHeaderList($uid),
            'count' => UserModel::getCountInfo($uid),
        ]);
        return $this->fetch();
    }

    /**
     * 获取某用户的订单列表
     */
    public function getOneorderList($uid, $page = 1, $limit = 20)
    {
        return Json::successful(StoreOrder::getOneorderList(compact('uid', 'page', 'limit')));
    }

    /**
     * 获取某用户的积分列表
     */
    public function getOneIntegralList($uid, $page = 1, $limit = 20)
    {
        return Json::successful(UserBillAdmin::getOneIntegralList(compact('uid', 'page', 'limit')));
    }

    /**
     * 获取某用户的积分列表
     */
    public function getOneSignList($uid, $page = 1, $limit = 20)
    {
        return Json::successful(UserBillAdmin::getOneSignList(compact('uid', 'page', 'limit')));
    }

    /**
     * 获取某用户的余额变动记录
     */
    public function getOneBalanceChangList($uid, $page = 1, $limit = 20)
    {
        return Json::successful(UserBillAdmin::getOneBalanceChangList(compact('uid', 'page', 'limit')));
    }

    /**
     * 用户禁用相关操作
     */
    protected function setUnStatus($uid=[])
    {
        if(!$uid){
            return true;
        }
        //关闭店铺
        UserModel::where('uid','in',$uid)->where('isshop',1)->update(['isshop'=>-1]);
        //关闭店铺
        \app\admin\model\shop\ShopApply::where('uid','in',$uid)->update(['status'=>-1]);
        //下架商品
        $where=[['mer_id','in',$uid]];
        \app\admin\model\store\StoreProduct::setUnshow($where);
        return true;
    }
}
