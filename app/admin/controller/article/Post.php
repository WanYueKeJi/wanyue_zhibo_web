<?php

namespace app\admin\controller\article;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\article\Post as PostModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 单页管理
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Post extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取分类列表
     *  @return json
     */
    public function get_list()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['order', '']
        ]);
        return Json::successlayui(PostModel::getList($where));
    }

    /**
     * 快速编辑
     * @param string $field
     * @param string $id
     * @param string $value
     */
    public function set_field($field = '', $id = '', $value = '')
    {
        $field == '' || $id == '' || $value == '' && Json::fail('缺少参数');
        if (PostModel::where('id', $id)->update([$field => $value])){
            return Json::successful('保存成功');
        }else{
            return Json::fail('保存失败');
        }

    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $id = $this->request->param('id');
        $news = [];
        $news['id'] = '';
        $news['title'] = '';
        $news['content'] = '';
        $news['sort'] = '0';

        if ($id) {
            $news = PostModel::where('id', $id)->field('*')->find();
            if (!$news) return $this->failed('数据不存在!');
            $news['content'] = htmlspecialchars_decode($news['content']);
        }

        $this->assign('news', $news);
        return $this->fetch();
    }

    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = Util::postMore([
            ['id', 0],
            'title',
            'content',
            ['visit', 0],
            ['sort', 0],
        ]);

        if ($data['id']) {
            $id = $data['id'];
            unset($data['id']);

            $res = PostModel::edit($data, $id, 'id');

            if ($res)
                return Json::successful('修改成功!', $id);
            else
                return Json::fail('修改失败', $id);
        } else {
            $data['add_time'] = time();
            $data['admin_id'] = $this->adminId;

            $res = PostModel::create($data);
            if ($res)
                return Json::successful('添加成功!', $res->id);
            else
                return Json::successful('添加失败!');
        }
    }


    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!PostModel::delid($id)){
            return Json::fail(PostModel::getErrorInfo('删除失败,请稍候再试!'));
        }else{
            return Json::successful('删除成功!');
        }

    }
}
