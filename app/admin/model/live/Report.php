<?php

namespace app\admin\model\live;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\models\user\User;

/**
 * Class ReportClass
 * @package app\admin\model\store
 */
class Report extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_report';

    use ModelTrait;

    public static $status = [0=>'待处理',1=>'已处理'];
    /**
     * 获取连表MOdel
     * @param $model
     * @return object
     */
    public static function getModelObject($where = [])
    {
        $model = new self();
        if (!empty($where)) {
            if (isset($where['uid']) && $where['uid'] != '') {
                $model = $model->where('uid', $where['uid']);
            }

            if (isset($where['touid']) && $where['touid'] != '') {
                $model = $model->where('touid', $where['touid']);
            }
        }
        return $model;
    }
    /**
     * 异步获取分类列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {

        $model = self::getModelObject($where);
        if(isset($where['page']) && $where['page']) $model = $model->page((int)$where['page'], (int)$where['limit']);
        $model = $model->order('id desc');

        $data = ($data =$model->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {
            $uid_name='已删除';
            $userinfo=User::getUserInfo($item['uid'],'uid,nickname');
            if($userinfo){
                $uid_name=$userinfo['nickname'].' ('.$userinfo['uid'].')';
            }
            $item['uid_name']=$uid_name;

            $touid_name='已删除';
            $liveuidinfo=User::getUserInfo($item['touid'],'uid,nickname');
            if($liveuidinfo){
                $touid_name=$liveuidinfo['nickname'].' ('.$liveuidinfo['uid'].')';
            }
            $item['touid_name']=$touid_name;

            $item['addtime']=date('Y-m-d H:i:s',$item['add_time']);
            $item['status_txt']=self::$status[$item['status']];
        }
        $count = self::count();
        return compact('count', 'data');
    }


    public static function delid($id)
    {
        return self::del($id);
    }

}