<?php
/**
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/11/28
 */

namespace app\admin\model\wechat;

use wanyue\basic\BaseModel;
use wanyue\traits\ModelTrait;
use think\facade\{Cache, Config};
use wanyue\services\{ PHPExcelService};

/**
 * 微信用户 model
 * Class WechatUser
 * @package app\admin\model\wechat
 */
class WechatUser extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'wechat_user';

    use ModelTrait;

    protected $insert = ['add_time'];


    public static function setAddTimeAttr($value)
    {
        return time();
    }

    /**
     * 获取微信用户
     * @param array $where
     * @return array
     */
    public static function systemPage($where = [], $isall = false)
    {
        $model = new self;
        $model = $model->where('openid|routine_openid', 'NOT NULL');
        if ($where['nickname'] !== '') $model = $model->where('nickname', 'LIKE', "%$where[nickname]%");
        if (isset($where['data']) && $where['data'] !== '') $model = self::getModelTime($where, $model, 'add_time');
        if (isset($where['tagid_list']) && $where['tagid_list'] !== '') {
            $tagid_list = explode(',', $where['tagid_list']);
            foreach ($tagid_list as $v) {
                $model = $model->where('tagid_list', 'LIKE', "%$v%");
            }
        }
        if (isset($where['groupid']) && $where['groupid'] !== '-1') $model = $model->where('groupid', "$where[groupid]");
        if (isset($where['sex']) && $where['sex'] !== '') $model = $model->where('sex', "$where[sex]");
        if (isset($where['subscribe']) && $where['subscribe'] !== '') $model = $model->where('subscribe', "$where[subscribe]");
        $model = $model->order('uid desc');
        if (isset($where['export']) && $where['export'] == 1) {
            $list = $model->select()->toArray();
            $export = [];
            foreach ($list as $index => $item) {
                $export[] = [
                    $item['nickname'],
                    $item['sex'],
                    $item['country'] . $item['province'] . $item['city'],
                    $item['subscribe'] == 1 ? '关注' : '未关注',
                ];
                $list[$index] = $item;
            }
            PHPExcelService::setExcelHeader(['名称', '性别', '地区', '是否关注公众号'])
                ->setExcelTile('微信用户导出', '微信用户导出' . time(), ' 生成时间：' . date('Y-m-d H:i:s', time()))
                ->setExcelContent($export)
                ->ExcelSave();
        }
        return self::page($model, function ($item) {
            $item['time'] = $item['add_time'] ? date('Y-m-d H:i', $item['add_time']) : '暂无';
        }, $where);
    }

}