<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow" />
    <title>登录管理系统 -  Powered by 万岳科技!</title>
    <link href="{__FRAME_PATH}css/bootstrap.min.css?v=3.4.0" rel="stylesheet">
    <link href="{__PLUG_PATH}layui/css/layui.css" rel="stylesheet">
    <link href="{__FRAME_PATH}css/font-awesome.min.css?v=4.3.0" rel="stylesheet">
    <link href="{__FRAME_PATH}css/animate.min.css" rel="stylesheet">
    <link href="{__FRAME_PATH}css/style.min.css?v=3.0.0" rel="stylesheet">
    <link href="{__ADMIN_PATH}css/login.css" rel="stylesheet">
    <script>
        top != window && (top.location.href = location.href);
    </script>
</head>
<body class="gray-bg login-bg">
<div class="login">
    <div class="login_left">
        <div class="login_title">
            <img src="/login_w.png"> 山东万岳科技
        </div>
        <div class="login_img">
            <img src="{__ADMIN_PATH}images/login/login_img.png">
        </div>
    </div>
    <div class="login_right">
        <div class="login_bd">
            <div class="bd_title">
                万岳直播带货系统
            </div>
            <form role="form" action="{:url('verify')}" method="post" id="form" onsubmit="return false">
            <div class="bd_input input-group m-b">
                <span class="input-group-addon"><i class="fa fa-user"></i> </span>
                <input type="text" id="account" name="account" placeholder="用户名" required="">
            </div>
            <div class="bd_input input-group m-b">
                <span class="input-group-addon"><i class="fa fa-unlock-alt"></i> </span>
                <input type="password" id="pwd" name="pwd" placeholder="密码" required="">
            </div>
            <div class="bd_input">
                <img id="verify_img" src="{:Url('captcha')}" alt="验证码" style="padding: 0;height: 50px;margin: 0;width: 100%;">
            </div>
            <div class="bd_input input-group m-b">
                <span class="input-group-addon"><i class="fa fa-shield"></i> </span>
                <input type="text" id="verify" name="verify" placeholder="验证码" required="">
            </div>
            <div class="bd_submit">
                <button type="submit" class="submitbtn">登 录</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="{__PLUG_PATH}jquery-1.10.2.min.js"></script>
<script src="{__FRAME_PATH}js/bootstrap.min.js?v=3.4.0"></script>
<!--<script src="{__MODULE_PATH}login/flaotfont.js"></script>-->
<script src="{__MODULE_PATH}login/ios-parallax.js"></script>
<script src="{__PLUG_PATH}layui/layui.all.js"></script>
<script src="{__MODULE_PATH}login/index.js"></script>
<!--统计代码，可删除-->
<!--点击刷新验证码-->
</body>
</html>