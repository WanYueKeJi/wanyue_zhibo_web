<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\user;

use app\models\user\User;
use wanyue\basic\BaseModel;
use wanyue\traits\ModelTrait;

/**
 * TODO 用户关注Model
 * Class StoreCart
 * @package app\models\store
 */
class UserAttent extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'user_attent';

    use  ModelTrait;

    //关注的用户列表
    public static function getFollow($uid,$page = 0, $limit = 0 )
    {
        if ($uid<1 || $page<1 || $limit<1) return [];


        $model = self::where('uid',$uid)->order('add_time desc');
        if ($page) $model->page($page, $limit);
        $list = $model->select()->toArray();

        foreach ($list as &$item){
            $nickname='';
            $avatar='';
            $item['uid']=$item['touid'];
            $userinfo=User::getUserInfo($item['uid'],'nickname,avatar');
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }
            $item['nickname']=$nickname;
            $item['avatar']=$avatar;

            unset($item['id']);
            unset($item['touid']);
            unset($item['add_time']);
        }

        return $list;
    }

    //关注的用户ID
    public static function getFollowIds($uid,$page = 0, $limit = 0 )
    {
        if ($uid<1 ) return [];

        $model = self::where('uid',$uid)->field('touid')->order('add_time desc');
        $list = $model->select()->toArray();

        if(!$list) return [];

        $uids=array_column($list,'touid');

        return $uids;
    }

    //粉丝用户列表
    public static function getFans($touid,$page = 0, $limit = 0 )
    {
        if ($touid<1 || $page<1 || $limit<1) return [];


        $model = self::where('touid',$touid)->order('add_time desc');
        if ($page) $model->page($page, $limit);
        $list = $model->select()->toArray();

        foreach ($list as &$item){
            $nickname='';
            $avatar='';
            $item['uid']=$item['uid'];
            $userinfo=User::getUserInfo($item['uid'],'nickname,avatar');
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }
            $item['nickname']=$nickname;
            $item['avatar']=$avatar;

            unset($item['id']);
            unset($item['touid']);
            unset($item['add_time']);
        }

        return $list;
    }

    //粉丝用户ID
    public static function getFansIds($touid )
    {
        if ($touid<1) return [];

        $model = self::where('touid',$touid)->field('uid')->order('add_time desc');
        $list = $model->select()->toArray();
        if(!$list) return [];

        $uids=array_column($list,'uid');

        return $uids;
    }

    public static function handelInfo($info )
    {
        if($info['thumb']=='') $info['thumb']='测试';
        $info['likes']=NumberFormat($info['likes']);
        $info['nums']=NumberFormat(0);
        $stream=$info['uid'].'_'.$info['showid'];
        $info['stream']=$stream;
        $info['pull']=self::getStreamUrl($stream);

        $nickname='';
        $avatar='';
        $userinfo=User::getUserInfo($info['uid'],'nickname,avatar');
        if($userinfo){
            $nickname=$userinfo['nickname'];
            $avatar=$userinfo['avatar'];
        }
        $info['nickname']=$nickname;
        $info['avatar']=$avatar;

        $goods_img='';
        if($info['goodsnum']>0){
            $goods_img='';
        }
        $info['goods_img']=$goods_img;

        return $info;
    }

    //粉丝数量
    public static function getFansNums($touid )
    {
        return self::where(['touid'=>$touid])->count();
    }

    //关注数量
    public static function getFollsNums($touid )
    {
        return self::where(['uid'=>$touid])->count();
    }

    //是否关注
    public static function isAttent($uid,$touid )
    {
        return self::be(['uid'=>$uid,'touid'=>$touid]) > 0 ? 1: 0;
    }

    //关注
    public static function addAttent($uid,$touid)
    {

        if($uid<0 || $touid<0) return self::setErrorInfo('信息错误');

        if(!self::isAttent($uid,$touid)){
            $add_time=time();
            $res=self::create(compact('uid','touid','add_time'));

            if(!$res ) return self::setErrorInfo('操作失败');
        }
        return true;

    }

    //取消关注
    public static function delAttent($uid,$touid)
    {

        if($uid<0 || $touid<0) return self::setErrorInfo('信息错误');

        $info=self::where('uid',$uid)->where('touid',$touid)->delete();

        if(!$info ) return self::setErrorInfo('操作失败');

        return true;

    }


}