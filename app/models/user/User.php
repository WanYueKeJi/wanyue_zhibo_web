<?php


namespace app\models\user;

use app\models\store\StoreOrder;
use app\models\store\StoreProduct;
use wanyue\services\SystemConfigService;
use think\facade\Session;
use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use wanyue\traits\JwtAuthModelTrait;
use app\Redis;

/**
 * TODO 用户Model
 * Class User
 * @package app\models\user
 */
class User extends BaseModel
{
    use JwtAuthModelTrait;
    use ModelTrait;

    protected $pk = 'uid';

    protected $name = 'user';

    protected $insert = ['add_time', 'add_ip', 'last_time', 'last_ip'];

    protected $hidden = [
        'add_ip', 'add_time', 'account', 'clean_time', 'last_ip', 'last_time', 'pwd', 'status', 'mark', 'pwd'
    ];

    protected function setAddTimeAttr($value)
    {
        return time();
    }

    protected function setAddIpAttr($value)
    {
        return app('request')->ip();
    }

    protected function setLastTimeAttr($value)
    {
        return time();
    }

    protected function setLastIpAttr($value)
    {
        return app('request')->ip();
    }

    public static function setWechatUser($wechatUser)
    {
        return self::create([
            'account' => 'wx' . $wechatUser['uid'] . time(),
            'pwd' => md5(123456),
            'nickname' => $wechatUser['nickname'] ?: '',
            'avatar' => $wechatUser['headimgurl'] ?: '',
            'add_time' => time(),
            'add_ip' => app('request')->ip(),
            'last_time' => time(),
            'last_ip' => app('request')->ip(),
            'uid' => $wechatUser['uid'],
            'user_type' => 'wechat'
        ]);
    }


    /**
     * TODO 获取会员是否被清除过的时间
     * @param $uid
     * @return int|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getCleanTime($uid)
    {
        $user = self::where('uid', $uid)->field(['add_time', 'clean_time'])->find();
        if (!$user) return 0;
        return $user['clean_time'] ? $user['clean_time'] : $user['add_time'];
    }

    /**
     * 更新用户信息
     * @param $wechatUser 用户信息
     * @param $uid 用户uid
     * @return bool|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function updateWechatUser($wechatUser, $uid)
    {
        $userInfo = self::where('uid', $uid)->find();
        if (!$userInfo) return;

            $data = [
            //    'nickname' => $wechatUser['nickname'] ?: '',
            //    'avatar' => $wechatUser['headimgurl'] ?: '',
                'login_type' => isset($wechatUser['login_type']) ? $wechatUser['login_type'] : $userInfo->login_type,
                'last_time' => time(),
                'last_ip' => request()->ip(),
            ];
            return self::edit($data, $uid, 'uid');
    }

    /**
     * 小程序用户添加
     * @param $routineUser
     * @return object
     */
    public static function setRoutineUser($routineUser)
    {
        self::beginTrans();
        $res1 = true;
//        $storeBrokerageStatu = sys_config('store_brokerage_statu') ? : 1;//获取后台分销类型
        $res2 = self::create([
            'account' => 'rt' . $routineUser['uid'] . time(),
            'pwd' => md5(123456),
            'nickname' => $routineUser['nickname'] ?: '',
            'avatar' => $routineUser['headimgurl'] ?: '',
            'uid' => $routineUser['uid'],
            'add_time' => $routineUser['add_time'],
            'add_ip' => request()->ip(),
            'last_time' => time(),
            'last_ip' => request()->ip(),
            'user_type' => $routineUser['user_type']
        ]);
        $res = $res1 && $res2;
        self::checkTrans($res);
        return $res2;
    }

    /**
     * 获得当前登陆用户UID
     * @return int $uid
     */
    public static function getActiveUid()
    {
        $uid = null;
        $uid = Session::get('LoginUid');
        if ($uid) return $uid;
        else return 0;
    }

    /**
     * TODO 查询当前用户信息
     * @param $uid $uid 用户编号
     * @param string $field $field 查询的字段
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getUserInfo($uid, $field = '')
    {
        if (strlen(trim($field))) $userInfo = self::where('uid', $uid)->field($field)->find();
        else  $userInfo = self::where('uid', $uid)->find();
        if (!$userInfo) return [];
        return $userInfo->toArray();
    }

    /**
     * 修改个人信息
     * @param $avatar 头像
     * @param $nickname 昵称
     * @param $uid 用户uid
     * @return bool
     */
    public static function editUser($avatar, $nickname, $uid)
    {
        return self::edit(['avatar' => $avatar, 'nickname' => $nickname], $uid, 'uid');
    }

    /**
     * H5用户注册
     * @param $account
     * @param $password
     * @return User|\think\Model
     */
    public static function register($account, $password,$source=0)
    {
        if (self::be(['account' => $account])) return self::setErrorInfo('用户已存在');
        $phone = $account;
        $data['account'] = $account;
        $data['pwd'] = md5($password);
        $data['phone'] = $phone;
        $data['real_name'] = '';
        $data['birthday'] = 0;
        $data['card_id'] = '';
        $data['mark'] = '';
        $data['addres'] = '';
        $data['user_type'] = 'h5';
        if($source==1){
            $data['user_type'] = 'Android';
        }
        if($source==2){
            $data['user_type'] = 'IOS';
        }
        $data['add_time'] = time();
        $data['add_ip'] = app('request')->ip();
        $data['last_time'] = time();
        $data['last_ip'] = app('request')->ip();
        $data['nickname'] = '用户'.substr(md5($account . time()), 0, 4);
        $data['avatar'] = $data['headimgurl'] = sys_config('h5_avatar');
        $data['city'] = '';
        $data['language'] = '';
        $data['province'] = '';
        $data['country'] = '';
        self::beginTrans();
        $res2 = WechatUser::create($data);
        $data['uid'] = $res2->uid;
        $res1 = self::create($data);
        $res = $res1 && $res2;
        self::checkTrans($res);
        return $res;
    }

    /**
     * 密码修改
     * @param $account
     * @param $password
     * @return User|bool
     */
    public static function reset($account, $password)
    {
        if (!self::be(['account' => $account])) return self::setErrorInfo('用户不存在');
        $count = self::where('account', $account)->where('pwd', md5($password))->count();
        if ($count) return true;
        return self::where('account', $account)->update(['pwd' => md5($password)]);
    }

    /**
     * 获取手机号是否注册
     * @param $phone
     * @return bool
     */
    public static function checkPhone($phone)
    {
        return self::be(['account' => $phone]);
    }


    /**
     * 自增
     *
     */
    public static function incField($uid, $parm=[])
    {
        if(!$parm) return 0;
        $model=self::where('uid', $uid);
        foreach ($parm as $k=>$v){
            $model=$model->inc($k,$v);
        }
        return $model->update();
    }

    /**
     * 自减
     */
    public static function decField($uid, $field,$value)
    {
        return self::where('uid', $uid)->where($field,'>=',$value)->dec($field,$value)->update();
    }

    /* 获取用户基本信息 redis */
    public static function getUserInfoByRedis($uid)
    {
        $key='userinfo_'.$uid;
        $userInfo=Redis::hGetAll($key);
        if(!$userInfo){
            $userInfo = self::where('uid', $uid)->field('uid,nickname,avatar')->find();
            if($userInfo){
                $userInfo=$userInfo->toArray();
                Redis::hMSet($key,$userInfo);
            }else{
                Redis::del($key);
            }
        }

        return $userInfo;
    }

    /* 更新 redis 用户信息 */
    public static function upUserInfoByRedis($uid,$data=[])
    {
        if(!$data){
            return 0;
        }
        $key='userinfo_'.$uid;
        $userInfo=Redis::hGetAll($key);
        if($userInfo){
            Redis::hMSet($key,$data);
        }
        return 1;
    }

}