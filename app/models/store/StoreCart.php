<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\store;

use app\admin\model\store\StoreProductAttrValue;
use app\admin\model\system\SystemGroupData;
use app\models\user\User;
use wanyue\basic\BaseModel;
use wanyue\services\UtilService;
use wanyue\traits\ModelTrait;

/**
 * TODO 购物车Model
 * Class StoreCart
 * @package app\models\store
 */
class StoreCart extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'store_cart';

    use ModelTrait;

    protected $insert = ['add_time'];

    protected function setAddTimeAttr()
    {
        return time();
    }

    public static function setCart($uid, $product_id, $cart_num = 1, $product_attr_unique = '', $type = 'product', $is_new = 0)
    {
        if ($cart_num < 1) $cart_num = 1;

            if (!$product_attr_unique) {
                $unique = StoreProduct::getSingleAttrUnique($product_id);
                if ($unique) {
                    $product_attr_unique = $unique;
                }
            }
            if (!StoreProduct::isValidProduct($product_id))
                return self::setErrorInfo('该产品已下架或删除');
            if (!StoreProductAttr::issetProductUnique($product_id, $product_attr_unique))
                return self::setErrorInfo('请选择有效的产品属性');
            if (StoreProduct::getProductStock($product_id, $product_attr_unique) < $cart_num)
                return self::setErrorInfo('该产品库存不足' . $cart_num);

        if ($cart = self::where('type', $type)->where('uid', $uid)->where('product_id', $product_id)->where('product_attr_unique', $product_attr_unique)->where('is_new', $is_new)->where('is_pay', 0)->where('is_del', 0)->find()) {
            if ($is_new)
                $cart->cart_num = $cart_num;
            else
                $cart->cart_num = bcadd($cart_num, $cart->cart_num, 0);
            $cart->add_time = time();
            $cart->save();
            return $cart;
        } else {
            $add_time = time();
            return self::create(compact('uid', 'product_id', 'cart_num', 'product_attr_unique', 'is_new', 'type', 'add_time'));
        }
    }

    public static function removeUserCart($uid, $ids)
    {
        return self::where('uid', $uid)->where('id', 'IN', implode(',', $ids))->update(['is_del' => 1]);
    }

    public static function getUserCartNum($uid, $type, $numType)
    {
        if ($numType) {
            return self::where('uid', $uid)->where('type', $type)->where('is_pay', 0)->where('is_del', 0)->where('is_new', 0)->count();
        } else {
            return self::where('uid', $uid)->where('type', $type)->where('is_pay', 0)->where('is_del', 0)->where('is_new', 0)->sum('cart_num');
        }
    }

    /**
     * TODO 修改购物车库存
     * @param $cartId
     * @param $cartNum
     * @param $uid
     * @return StoreCart|bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function changeUserCartNum($cartId, $cartNum, $uid)
    {
        $count = self::where('uid', $uid)->where('id', $cartId)->count();
        if (!$count) return self::setErrorInfo('参数错误');
        $cartInfo = self::where('uid', $uid)->where('id', $cartId)->field('product_id,product_attr_unique,cart_num')->find()->toArray();
        $stock = 0;
        if ($cartInfo['product_id']) {
            //TODO 获取普通产品的库存
            $stock = StoreProduct::getProductStock($cartInfo['product_id'], $cartInfo['product_attr_unique']);
        }
        if (!$stock) return self::setErrorInfo('暂无库存');
        if (!$cartNum) return self::setErrorInfo('库存错误');
        if ($stock < $cartNum) return self::setErrorInfo('库存不足' . $cartNum);
        if ($cartInfo['cart_num'] == $cartNum) return true;
        return self::where('uid', $uid)->where('id', $cartId)->update(['cart_num' => $cartNum]);
    }

    public static function getUserProductCartList($uid, $cartIds = '', $status = 0)
    {
        $productInfoField = 'id,image,price,ot_price,vip_price,postage,give_integral,sales,stock,store_name,unit_name,is_show,is_del,is_postage,cost,is_sub,temp_id,mer_id';

        $model = new self();
        $valid = $invalid = [];
        $shop = $shoplist = [];
        $model = $model->where('uid', $uid)->where('type', 'product')->where('is_pay', 0)
            ->where('is_del', 0);
        if (!$status) $model = $model->where('is_new', 0);
        if ($cartIds) $model = $model->where('id', 'IN', $cartIds);
        $model = $model->order('add_time DESC');
        $list = $model->select()->toArray();
        if (!count($list)) return compact('valid', 'invalid');
        $now = time();
        foreach ($list as $k => $cart) {

                $product = StoreProduct::field($productInfoField)
                    ->find($cart['product_id'])->toArray();

            $product['image'] = set_file_url($product['image']);
            $cart['productInfo'] = $product;

            //商品不存在
            if (!$product) {
                $model->where('id', $cart['id'])->update(['is_del' => 1]);
                //商品删除或无库存
            } else if (!$product['is_show'] || $product['is_del'] || !$product['stock']) {
                $invalid[] = $cart;

                //秒杀产品未开启或者已结束
            } else if (!StoreProductAttr::issetProductUnique($cart['product_id'], $cart['product_attr_unique'])  ) {
                $invalid[] = $cart;
                //正常商品
            } else {

                if ($cart['product_attr_unique']) {
                    $attrInfo = StoreProductAttr::uniqueByAttrInfo($cart['product_attr_unique']);
                    //商品没有对应的属性
                    if (!$attrInfo || !$attrInfo['stock'])
                        $invalid[] = $cart;
                    else {
                        $cart['productInfo']['attrInfo'] = $attrInfo;

                            $cart['truePrice'] = (float)StoreProduct::setLevelPrice($attrInfo['price'], $uid, true);
                            $cart['vip_truePrice'] = (float)StoreProduct::setLevelPrice($attrInfo['price'], $uid);

                        $cart['trueStock'] = $attrInfo['stock'];
                        $cart['costPrice'] = $attrInfo['cost'];
                        $cart['productInfo']['image'] = empty($attrInfo['image']) ? $cart['productInfo']['image'] : $attrInfo['image'];
                        $valid[] = $cart;
                    }
                } else {

                        $cart['truePrice'] = (float)StoreProduct::setLevelPrice($cart['productInfo']['price'], $uid, true);
                        $cart['vip_truePrice'] = (float)StoreProduct::setLevelPrice($cart['productInfo']['price'], $uid);

                    $cart['trueStock'] = $cart['productInfo']['stock'];
                    $cart['costPrice'] = $cart['productInfo']['cost'];
                    $valid[] = $cart;
                }
            }
        }
        foreach ($valid as $k => $cart) {
            if ($cart['trueStock'] < $cart['cart_num']) {
                $cart['cart_num'] = $cart['trueStock'];
                $model->where('id', $cart['id'])->update(['cart_num' => $cart['cart_num']]);
                $valid[$k] = $cart;
            }

            unset($valid[$k]['uid'], $valid[$k]['is_del'], $valid[$k]['is_new'], $valid[$k]['is_pay'], $valid[$k]['add_time']);
            if (isset($valid[$k]['productInfo'])) {
                unset($valid[$k]['productInfo']['is_del'], $valid[$k]['productInfo']['is_del'], $valid[$k]['productInfo']['is_show']);
            }
        }
        foreach ($invalid as $k => $cart) {
            unset($valid[$k]['uid'], $valid[$k]['is_del'], $valid[$k]['is_new'], $valid[$k]['is_pay'], $valid[$k]['add_time']);
            if (isset($invalid[$k]['productInfo'])) {
                unset($invalid[$k]['productInfo']['is_del'], $invalid[$k]['productInfo']['is_del'], $invalid[$k]['productInfo']['is_show']);
            }
        }

        //根据店铺处理商品
        //if($status==0) {
            foreach ($valid as $k => $v) {
                $shop[$v['productInfo']['mer_id']][] = $v;
            }
            foreach ($shop as $k => $v) {
                $name = '平台自营';
                if ($k > 0) {
                    $mer_info = User::getUserInfo($k, 'nickname');
                    if(!$mer_info){
                        //店铺用户已删除
                        continue;
                    }

                    $name = $mer_info['nickname'] . '的小店';

                }

                $data = [
                    'mer_id' => $k,
                    'name' => $name,
                    'list' => $v,
                ];
                $shoplist[] = $data;
            }
            $valid = $shoplist;
            //根据店铺处理商品
        //}
        return compact('valid', 'invalid');
    }

    /**
     * 产品编号
     * @param array $ids
     * @return array
     */
    public static function getCartIdsProduct(array $ids)
    {
        return self::whereIn('id', $ids)->column('product_id', 'id');
    }

    /**
     *  获取购物车内最新一张产品图
     */
    public static function getProductImage(array $cart_id)
    {
        return self::whereIn('a.id', $cart_id)->alias('a')->order('a.id desc')
            ->join('store_product p', 'p.id = a.product_id')->value('p.image');
    }

}