<?php

namespace wanyue\services\qrcode;

use wanyue\services\apply\Apply;
use wanyue\traits\LogicTrait;

/**
 * Class Qrcode
 * @package wanyue\services\qrcode
 * @method $this wechatApp()
 * 调用示例：Qrcode::instance()->wechatApp()->test();
 */
class Qrcode extends Apply
{
    use LogicTrait;

}