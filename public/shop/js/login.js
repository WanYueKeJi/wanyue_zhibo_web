(function(global,$){
    $(document).ready(function() {
        var issub=0;
        var layer = null;
        layui.use(['layer'], function () {
            layer = layui.layer;
        })
        var timer=null;
        function setTimer(obj){
            var time_t=60;
            timer=window.setInterval(function() {
                if (time_t > 0) {
                    var i = time_t--+"s 重新获取";
                    obj.addClass("waiting").text(i);
                } else {
                    window.clearInterval(timer);
                    timer = null;
                    obj.removeClass("waiting").text('获取验证码');
                }
            },1e3);
        }

        $('.getcode').on('click',function(){
            var _this=$(this);
            var account = $('#account').val();

            if(!account) return layer.msg('请输入登陆账号');

            if(_this.hasClass('waiting')){
                return !1;
            }
            if(issub==1){
                return !1;
            }
            issub=1;
            $.ajax({
                url: '/api/verify_code',
                data: {},
                type: 'get',
                dataType: 'json',
                success: function (rem) {
                    if (rem.code == 200 || rem.status == 200) {
                        $.ajax({
                            url: '/api/register/verify',
                            data: {
                                type:'login',
                                phone:account,
                                key:rem.data.key,
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (rem) {
                                issub=0;
                                if (rem.code == 200 || rem.status == 200) {
                                    layer.msg(rem.msg);
                                    setTimer(_this);
                                }else{
                                    layer.msg(rem.msg);
                                }
                            },
                            error: function (err) {
                                issub=0;
                                layer.msg('系统错误');
                            }
                        })
                    }else{
                        issub=0;
                        layer.msg(rem.msg);
                    }
                },
                error: function (err) {
                    layer.msg('系统错误');
                }
            })


            return false;
        })

        $('.submitbtn').on('click',function(){
            var account = $('#account').val(),captcha = $('#captcha').val(),verify = $('#verify').val();

            if(!account) return layer.msg('请输入登陆账号');
            if(!captcha) return layer.msg('请输入验证码');
            $.ajax({
                url: '/shop/login/verify',
                data: {
                    account:account,
                    captcha:captcha
                },
                type: 'post',
                dataType: 'json',
                success: function (rem) {
                    if (rem.code == 200 || rem.status == 200) {
                        window.location.href = rem.data.url || '/shop/index/index.html';
                    }else{
                        layer.msg(rem.msg);
                    }
                },
                error: function (err) {
                    layer.msg('系统错误');
                }
            })
            return false;
        })

    });

})(window,jQuery);